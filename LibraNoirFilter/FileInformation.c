/*++

Module Name:

	FileInformation.c

Abstract:

	This is the file information module of the miniFilter driver.

Environment:

	Kernel mode

--*/

#include "FileInformation.h"

NTSTATUS
GetFilePath(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_Out_ PUNICODE_STRING Filepath
)
/*++

Routine Description:
	
	This is getting file full path for minifilter driver callback routine.

Arguments:

	Data - Pointer to the filter callbackData that is passed to us.

	Filepath - Pointer to unicode string that file path will be saved.

Return Value:

	Routine can return non success error codes.

--*/
{
	NTSTATUS status;
	POBJECT_NAME_INFORMATION volumeName = NULL;

	status = IoQueryFileDosDeviceName(Data->Iopb->TargetFileObject, &volumeName);

	if (!NT_SUCCESS(status)) {
		if (volumeName) ExFreePool(volumeName);
		volumeName = NULL;
		return status;
	}

	status = RtlUnicodeStringCopy(Filepath, volumeName);

	if (volumeName) ExFreePool(volumeName);
	volumeName = NULL;

	if (!NT_SUCCESS(status)) {
		return status;
	}

	status = RtlAppendUnicodeStringToString(Filepath, &Data->Iopb->TargetFileObject->FileName);

	if (!NT_SUCCESS(status)) {
		return status;
	}

	return status;
}

BOOLEAN
IsFileWriteFunction(
	_Inout_ PFLT_CALLBACK_DATA Data
)
/*++

Routine Description:

	This is checking the request is writing file on callback routine

Arguments:

	Data - Pointer to the filter callbackData that is passed to us.

Return Value:

	Routine can return TRUE or FALSE

--*/
{
	if (Data->Iopb->MajorFunction == IRP_MJ_CREATE &&
		Data->Iopb->Parameters.Create.SecurityContext->DesiredAccess & (FILE_WRITE_DATA | FILE_APPEND_DATA)) {

		return TRUE;
	}

	return FALSE;
}

BOOLEAN
IsFileDeleteFunction(
	_Inout_ PFLT_CALLBACK_DATA Data
)
/*++

Routine Description:

	This is checking the request is deleting file on callback routine

Arguments:

	Data - Pointer to the filter callbackData that is passed to us.

Return Value:

	Routine can return TRUE or FALSE

--*/
{
	if (Data->Iopb->MajorFunction == IRP_MJ_SET_INFORMATION &&
		Data->Iopb->Parameters.SetFileInformation.FileInformationClass == FileDispositionInformation &&
		((FILE_DISPOSITION_INFORMATION*)Data->Iopb->Parameters.SetFileInformation.InfoBuffer)->DeleteFile) {

		return TRUE;
	}

	return FALSE;
}

BOOLEAN
GetFileExtension(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_Out_ PUNICODE_STRING Extension
)
/*++

Routine Description:

	This is getting file extension string on callback routine.

Arguments:

	Data - Pointer to the filter callbackData that is passed to us.

	Extension - Pointer to the unicode string that file extension is saved.

Return Value:

	Routine returns whether function succeed.

--*/
{
	PFLT_FILE_NAME_INFORMATION nameInfo;

	if (NT_SUCCESS(FltGetFileNameInformation(
		Data,
		FLT_FILE_NAME_NORMALIZED |
		FLT_FILE_NAME_QUERY_ALWAYS_ALLOW_CACHE_LOOKUP,
		&nameInfo
	))) {

		if (NT_SUCCESS(FltParseFileNameInformation(nameInfo))) {

			RtlUnicodeStringCopy(Extension, &nameInfo->Extension);
			return TRUE;
		}

		FltReleaseFileNameInformation(nameInfo);
	}

	return FALSE;
}