/*++

Module Name:

	FileInformation.h

Abstract:

	This is the file information module of the miniFilter driver.

Environment:

	Kernel mode

--*/

#pragma once

#include <fltKernel.h>
#include <ntstrsafe.h>


/*************************************************************************
	Prototypes
*************************************************************************/

NTSTATUS
GetFilePath(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_Out_ PUNICODE_STRING filepath
);

BOOLEAN
IsFileWriteFunction(
	_Inout_ PFLT_CALLBACK_DATA Data
);

BOOLEAN
IsFileDeleteFunction(
	_Inout_ PFLT_CALLBACK_DATA Data
);

BOOLEAN
GetFileExtension(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_Out_ PUNICODE_STRING Extension
);