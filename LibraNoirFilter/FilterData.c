/*++

Module Name:

	FilterData.c

Abstract:

	This is the filtering data module of the miniFilter driver.

Environment:

	Kernel mode

--*/

#include <string.h>

#include "FilterData.h"

#define TrustedSystemProcessCount (sizeof(TrustedSystemProcess) / sizeof(LPCSTR))

/*
	GLOBAL VARIABLES
*/

LPCSTR TrustedSystemProcess[] = {
	// LibraNoir User Process
	{"libranoir.exe"},

	// Trusted System Process
	{"explorer.exe"},
	{"svchost.exe"},
	{"backgroundTask"},
	{"WMIADAP.exe"},
	{"MpCmdRun.exe"},
	{"MsMpEng.exe"},
	{"FileCoAuth.exe"},
	{"TrustedInstall"},
	{"TiWorker.exe"},
	{"Provtool.exe"},
	{"Microsoft.Phot"},
	{"RuntimeBroker."},
	{"Taskhostw.exe"},
	{"sppsvc.exe"},
	{"lsass.exe"},
	{"provtool.exe"},
	{"mcbuilder.exe"},
	{"MPSigStub.exe"},
	{"SrTasks.exe"}
};

static int FilteringFileExtensionCount = 0;
UNICODE_STRING FilteringFileExtension[50];

/*
	FUNCTIONS
*/
BOOLEAN
IsTrustedSystemProcess(
	_In_ LPCSTR Requestor
)
/*++

Routine Description:

	This is checking the requestor process is trusted system process.

Arguments:

	Requestor - Pointer to the requestor process name string.

Return Value:

	TRUE - Requestor is Trusted System Process.

	FALSE - Requestor is NOT Trusted System Process.

--*/
{
	int i;

	for (i = 0; i < TrustedSystemProcessCount; i++) {

		if (strcmp(TrustedSystemProcess[i], Requestor) == 0)
			return TRUE;
	}
	return FALSE;
}

BOOLEAN
AddFilteringFileExtension(
	_In_ PUNICODE_STRING FileExtension
)
/*++

Routine Description:

	This is adding the filtering file extension information routine.

Arguments:

	FileExtension - Pointer to the file extension unicode string.

Return Value:

	TRUE - Add Succeed.

	FALSE - Add Failed.

--*/
{
	// Array is Full
	if (FilteringFileExtensionCount == 50) return FALSE;

	// Already Exist
	if (IsFilteringFileExtension(FileExtension) != -1) return FALSE;

	AllocUnicodeString(&FilteringFileExtension[FilteringFileExtensionCount], 30 * sizeof(WCHAR));

	if (FilteringFileExtension[FilteringFileExtensionCount].Buffer == NULL) 
		return FALSE;

	if (!NT_SUCCESS(RtlUnicodeStringCopy(&FilteringFileExtension[FilteringFileExtensionCount], FileExtension)))
		return FALSE;

	DbgPrint("LibraNoirFilter: File Extension [%wZ] Added\n", FilteringFileExtension[FilteringFileExtensionCount]);

	FilteringFileExtensionCount++;
	return TRUE;
}

BOOLEAN
RemoveFilteringFileExtension(
	_In_ PUNICODE_STRING FileExtension
)
/*++

Routine Description:

	This is removing the filtering file extension information routine.

Arguments:

	FileExtension - Pointer to the file extension unicode string.

Return Value:

	TRUE - Remove Succeed.

	FALSE - Remove Failed.

--*/
{
	int i;

	if (FilteringFileExtensionCount == 0) return FALSE;

	i = IsFilteringFileExtension(FileExtension);

	if (i == -1) return FALSE;

	DbgPrint("File Extension [%wZ] Removed\n", FilteringFileExtension[i]);

	FreeUnicodeString(&FilteringFileExtension[i]);

	FilteringFileExtension[i] = FilteringFileExtension[FilteringFileExtensionCount - 1];
	FilteringFileExtension[FilteringFileExtensionCount - 1].Buffer = NULL;
	FilteringFileExtension[FilteringFileExtensionCount - 1].Length = 0;
	FilteringFileExtension[FilteringFileExtensionCount - 1].MaximumLength = 0;
	FilteringFileExtensionCount--;

	return TRUE;
}

INT
IsFilteringFileExtension(
	_In_ PUNICODE_STRING FileExtension
)
/*++

Routine Description:

	This is checking the filtering file extension information existence.

Arguments:

	FileExtension - Pointer to the file extension unicode string.

Return Value:

	INDEX on Array (0-49) - Exist.

	-1 - NOT Exist or Array is Empty.

--*/
{
	int i;

	if (FilteringFileExtensionCount == 0) return -1;

	for (i = 0; i < FilteringFileExtensionCount; i++) {

		if (RtlEqualUnicodeString(&FilteringFileExtension[i], FileExtension, TRUE)) {

			return i;
		}
	}
	return -1;
}

VOID
InitMSGStruct(
	_In_ PSEND_MSG Msg
)
{
	memset(Msg->ProcessName, 0, 40 * sizeof(CHAR));
	Msg->ProcessId = 0;
	memset(Msg->FileExtension, 0, 30 * sizeof(WCHAR));
	memset(Msg->FilePath, 0, 200 * sizeof(WCHAR));
}

VOID
InitRPYStruct(
	_In_ PREPLY_MSG Msg
)
{
	Msg->IsApproved = FALSE;
}

VOID
InitRCVStruct(
	_In_ PRCV_MSG Msg
)
{
	memset(Msg->FileExtension, 0, 30 * sizeof(WCHAR));
}