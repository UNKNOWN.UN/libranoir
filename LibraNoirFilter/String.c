/*++

Module Name:

	String.c

Abstract:

	This is the string module of the miniFilter driver.

Environment:

	Kernel mode

--*/

#include "String.h"

VOID
AllocUnicodeString(
	_Out_ PUNICODE_STRING String,
	_In_ USHORT Size
)
/*++

Routine Description:

	This is Allocating memory from memory pool for unicode string.

Arguments:

	String - Pointer to unicode string that needs memory allocation.

	Size - Value to size of memory allocation.

Return Value:

	VOID

--*/
{
	/* invaild parametor */
	if (!String) return;

	__try {
		String->Length = 0;
		String->MaximumLength = 0;
		String->Buffer = NULL;

		String->Buffer = (PWSTR)ExAllocatePool(NonPagedPool, Size);
		if (String->Buffer) {
			String->MaximumLength = Size;
			String->Length = 0;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER) { String->Buffer = NULL; }
}

VOID
FreeUnicodeString(
	_Out_ PUNICODE_STRING String
)
/*++

Routine Description:

	This is Deallocating memory for unicode string.

Arguments:

	String - Pointer to unicode string that needs memory deallocation.

Return Value:

	VOID

--*/
{
	/* invaild parametor */
	if (!String) return;

	__try {
		if (String->Buffer) {
			ExFreePool(String->Buffer);
			String->Buffer = NULL;
		}
		String->Length = 0;
		String->MaximumLength = 0;
	}
	__except (EXCEPTION_EXECUTE_HANDLER) { }
}