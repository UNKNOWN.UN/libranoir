/*++

Module Name:

	String.h

Abstract:

	This is the string module of the miniFilter driver.

Environment:

	Kernel mode

--*/

#pragma once

#include <fltKernel.h>
#include <ntstrsafe.h>

/*************************************************************************
	Prototypes
*************************************************************************/

VOID
AllocUnicodeString(
	_Out_ PUNICODE_STRING String,
	_In_ USHORT Size
);

VOID
FreeUnicodeString(
	_Out_ PUNICODE_STRING String
);