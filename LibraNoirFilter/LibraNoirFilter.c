/*++

Module Name:

	LibraNoirFilter.c

Abstract:

	This is the main module of the LibraNoirFilter miniFilter driver.
	
Environment:

	Kernel mode

--*/
#include <fltKernel.h>
#include <ntstrsafe.h>

#include "LibraNoirFilter.h"

#pragma prefast(disable:__WARNING_ENCODE_MEMBER_FUNCTION_POINTER, "Not valid for kernel mode drivers")


/*************************************************************************
	Global Variables
*************************************************************************/

LIBRANOIR_DATA LibraNoirData;


/*************************************************************************
	Functions
*************************************************************************/

extern UCHAR *PsGetProcessImageFileName(IN PEPROCESS Process);


NTSTATUS
LibraNoirFilterInstanceSetup(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_SETUP_FLAGS Flags,
	_In_ DEVICE_TYPE VolumeDeviceType,
	_In_ FLT_FILESYSTEM_TYPE VolumeFilesystemType
)
/*++

Routine Description:

	This routine is called whenever a new instance is created on a volume. This
	gives us a chance to decide if we need to attach to this volume or not.

	If this routine is not defined in the registration structure, automatic
	instances are always created.

Arguments:

	FltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
		opaque handles to this filter, instance and its associated volume.

	Flags - Flags describing the reason for this attach request.

Return Value:

	STATUS_SUCCESS - attach
	STATUS_FLT_DO_NOT_ATTACH - do not attach

--*/
{
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(Flags);
	UNREFERENCED_PARAMETER(VolumeFilesystemType);

	PAGED_CODE();

	DbgPrint("LibraNoirFilter!LibraNoirFilterInstanceSetup: Entered\n");

	//
	//	We don't filtering CD-ROM
	//
	return (VolumeDeviceType != FILE_DEVICE_CD_ROM_FILE_SYSTEM) ?
		STATUS_SUCCESS :
		STATUS_FLT_DO_NOT_ATTACH;
}


NTSTATUS
LibraNoirFilterInstanceQueryTeardown(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_QUERY_TEARDOWN_FLAGS Flags
)
/*++

Routine Description:

	This is called when an instance is being manually deleted by a
	call to FltDetachVolume or FilterDetach thereby giving us a
	chance to fail that detach request.

	If this routine is not defined in the registration structure, explicit
	detach requests via FltDetachVolume or FilterDetach will always be
	failed.

Arguments:

	FltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
		opaque handles to this filter, instance and its associated volume.

	Flags - Indicating where this detach request came from.

Return Value:

	Returns the status of this operation.

--*/
{
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(Flags);

	PAGED_CODE();

	DbgPrint("LibraNoirFilter!LibraNoirFilterInstanceQueryTeardown: Entered\n");

	return STATUS_SUCCESS;
}


/*************************************************************************
	MiniFilter initialization and unload routines.
*************************************************************************/

NTSTATUS
DriverEntry(
	_In_ PDRIVER_OBJECT DriverObject,
	_In_ PUNICODE_STRING RegistryPath
)
/*++

Routine Description:

	This is the initialization routine for this miniFilter driver.  This
	registers with FltMgr and initializes all global data structures.

Arguments:

	DriverObject - Pointer to driver object created by the system to
		represent this driver.

	RegistryPath - Unicode string identifying where the parameters for this
		driver are located in the registry.

Return Value:

	Routine can return non success error codes.

--*/
{
	NTSTATUS status;

	OBJECT_ATTRIBUTES oa;
	UNICODE_STRING portName;
	PSECURITY_DESCRIPTOR sd;

	UNICODE_STRING temp;

	DbgPrint("LibraNoirFilter!DriverEntry: Entered\n");
	DbgPrint("LibraNoirFilter!RegistryPath: %wZ\n", RegistryPath);

	LibraNoirData.DriverObject = DriverObject;

	//
	//  Register with FltMgr to tell it our callback routines
	//

	status = FltRegisterFilter(DriverObject,
							   &FilterRegistration,
							   &LibraNoirData.Filter);

	FLT_ASSERT(NT_SUCCESS(status));

	
	if (NT_SUCCESS(status)) {

		/* OPEN FltCreateCommunicationPort */

		RtlInitUnicodeString(&portName, LIBRA_NOIR_FILTER_COMMUNICATION_PORT_NAME);

		status = FltBuildDefaultSecurityDescriptor(&sd, FLT_PORT_ALL_ACCESS);

		if (NT_SUCCESS(status)) {

			InitializeObjectAttributes(&oa,
									   &portName,
									   OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
									   NULL,
									   sd);

			status = FltCreateCommunicationPort(LibraNoirData.Filter,
												&LibraNoirData.ServerPort,
												&oa,
												NULL,
												LibraNoirFilterPortConnect,
												LibraNoirFilterPortDisconnect,
												LibraNoirFilterMessage,
												1);
			/*
				Free the security descriptor in all cases. It is not needed once
				the call to FltCreateCommunicationPort() is made.
			*/
			FltFreeSecurityDescriptor(sd);

			if (!NT_SUCCESS(status)) {
				
				DbgPrint("LibraNoirFilter!DriverEntry: fail to open port\n");
				FltCloseCommunicationPort(LibraNoirData.ServerPort);
				return status;
			}

			DbgPrint("LibraNoirFilter!Communication Port Open\n");
		}

		//
		//  Start filtering i/o
		//
		status = FltStartFiltering(LibraNoirData.Filter);

		if (!NT_SUCCESS(status)) {

			FltUnregisterFilter(LibraNoirData.Filter);
		}
	}

	return status;
}

NTSTATUS
LibraNoirFilterUnload(
	_In_ FLT_FILTER_UNLOAD_FLAGS Flags
)
/*++

Routine Description:

	This is the unload routine for this miniFilter driver. This is called
	when the minifilter is about to be unloaded. We can fail this unload
	request if this is not a mandatory unload indicated by the Flags
	parameter.

Arguments:

	Flags - Indicating if this is a mandatory unload.

Return Value:

	Returns STATUS_SUCCESS.

--*/
{
	UNREFERENCED_PARAMETER(Flags);

	PAGED_CODE();

	DbgPrint("LibraNoirFilter!LibraNoirFilterUnload: Entered\n");

	FltCloseCommunicationPort(LibraNoirData.ServerPort);
	FltUnregisterFilter(LibraNoirData.Filter);

	return STATUS_SUCCESS;
}


/*************************************************************************
	MiniFilter callback routines.
*************************************************************************/
FLT_PREOP_CALLBACK_STATUS
LibraNoirFilterPreOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
/*++

Routine Description:

	This routine is a pre-operation dispatch routine for this miniFilter.

	This is non-pageable because it could be called on the paging path

Arguments:

	Data - Pointer to the filter callbackData that is passed to us.

	FltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
		opaque handles to this filter, instance, its associated volume and
		file object.

	CompletionContext - The context for the completion routine for this
		operation.

Return Value:

	The return value is the status of the operation.

--*/
{
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(CompletionContext);

	REQUEST_DATA requestData;

	SEND_MSG sendData;
	REPLY_MSG replyData;
	ULONG rpyLen = sizeof(REPLY_MSG);

	NTSTATUS status;

	if (FLT_IS_IRP_OPERATION(Data)) {

		if (IsFileWriteFunction(Data) || /* Open file for writing/appending? */
			IsFileDeleteFunction(Data)) { /* Deleting file? */

			/* Get Process Information */
			requestData.RequestorPid = FltGetRequestorProcessId(Data);
			requestData.RequestorProcessName = (LPSTR)PsGetProcessImageFileName(FltGetRequestorProcess(Data));

			/* TRUSTED SYSTEM PROCESS WILL BE SKIPPED */
			if (IsTrustedSystemProcess(requestData.RequestorProcessName)) {

				//DbgPrint("LibraNoirFilter: Trusted System Process [%lu:%s]", 
				//	requestData.RequestorPid,
				//	requestData.RequestorProcessName);

				return FLT_PREOP_SUCCESS_NO_CALLBACK;
			}
				
			/* file extension check */
			AllocUnicodeString(&requestData.FileExtension, 30 * sizeof(WCHAR));
			GetFileExtension(Data, &requestData.FileExtension);

			if (IsFilteringFileExtension(&requestData.FileExtension) == -1) {

				//DbgPrint("LibraNoirFilter: File Extension not Filtering [%lu:%s] [%wZ]", 
				//	requestData.RequestorPid,
				//	requestData.RequestorProcessName,
				//	requestData.FileExtension);

				FreeUnicodeString(&requestData.FileExtension);
				return FLT_PREOP_SUCCESS_NO_CALLBACK;
			}

			/* Get File Information */
			AllocUnicodeString(&requestData.FilePath, 200 * sizeof(WCHAR));
			GetFilePath(Data, &requestData.FilePath);

			// TODO: SEND DATA TO USERMODE APP

			if (LibraNoirData.ClientPort != NULL) {

				InitMSGStruct(&sendData);
				InitRPYStruct(&replyData);

				sendData.ProcessId = requestData.RequestorPid;
				strcpy(sendData.ProcessName, requestData.RequestorProcessName);
				wcscpy(sendData.FileExtension, requestData.FileExtension.Buffer);
				wcscpy(sendData.FilePath, requestData.FilePath.Buffer);
				
				status = FltSendMessage(
					LibraNoirData.Filter,
					&LibraNoirData.ClientPort,
					&sendData,
					sizeof(SEND_MSG),
					&replyData,
					&rpyLen,
					0
				);

				if (!NT_SUCCESS(status))
					DbgPrint("LibraNoirFilter: SEND DATA FAIL!\n");

				// check the approval
				if (replyData.IsApproved == FALSE) {

					DbgPrint("LibraNoirFilter: Denied!\n");
					FltCancelIo(Data);
				}
			}

			DbgPrint("LibraNoirFilter: [%lu:%s] [%wZ] [%wZ] File Write/Delete Detected",
				requestData.RequestorPid,
				requestData.RequestorProcessName,
				requestData.FileExtension,
				requestData.FilePath);

			FreeUnicodeString(&requestData.FilePath);
			FreeUnicodeString(&requestData.FileExtension);
		}
	}

	/* callback success and do not call postoperation callback function */
	return FLT_PREOP_SUCCESS_NO_CALLBACK;
}

NTSTATUS
LibraNoirFilterPortConnect(
	_In_ PFLT_PORT ClientPort,
	_In_opt_ PVOID ServerPortCookie,
	_In_reads_bytes_opt_(SizeOfContext) PVOID ConnectionContext,
	_In_ ULONG SizeOfContext,
	_Outptr_result_maybenull_ PVOID *ConnectionCookie
)
/*++

	Routine Description

		This is called when user-mode connects to the server port - to establish a
		connection

	Arguments

		ClientPort - This is the client connection port that will be used to
			send messages from the filter

		ServerPortCookie - The context associated with this port when the
			minifilter created this port.

		ConnectionContext - Context from entity connecting to this port (most likely
			your user mode service)

		SizeofContext - Size of ConnectionContext in bytes

		ConnectionCookie - Context to be passed to the port disconnect routine.

	Return Value

		STATUS_SUCCESS - to accept the connection

--*/
{
	PAGED_CODE();

	UNREFERENCED_PARAMETER(ServerPortCookie);
	UNREFERENCED_PARAMETER(ConnectionContext);
	UNREFERENCED_PARAMETER(SizeOfContext);
	UNREFERENCED_PARAMETER(ConnectionCookie);

	LibraNoirData.UserProcess = IoGetCurrentProcess();
	LibraNoirData.ClientPort = ClientPort;

	DbgPrint("LibraNoirFilter!PortConnect: Client Port=0x%p\n", ClientPort);

	return STATUS_SUCCESS;
}

VOID
LibraNoirFilterPortDisconnect(
	_In_opt_ PVOID ConnectionCookie
)
/*++

	Routine Description

		This is called when the connection is torn-down. We use it to close our
		handle to the connection

	Arguments

		ConnectionCookie - Context from the port connect routine

	Return value

		None

--*/
{
	PAGED_CODE();

	UNREFERENCED_PARAMETER(ConnectionCookie);

	DbgPrint("LibraNoirFilter!PortDisConnect: Client Port=0x%p!\n", LibraNoirData.ClientPort);

	FltCloseClientPort(LibraNoirData.Filter, &LibraNoirData.ClientPort);
	LibraNoirData.UserProcess = NULL;
}

NTSTATUS
LibraNoirFilterMessage(
	_In_ PVOID PortCookie,
	_In_opt_ PVOID InputBuffer,
	_In_ ULONG InputBufferLength,
	_Out_opt_ PVOID OutputBuffer,
	_In_ ULONG OutputBufferLength,
	_Out_ PULONG ReturnOutputBufferLength
)
/*++

	Routine Description:

		This routine is called whenever the user program sends message to
		filter via FilterSendMessage(...).

		The user program sends message to
		
		1) Add file extension to filter.
		2) Remove file extension to filter.
		3) Install Process Proctection Routine.
		4) UnInstall Process Protection Routine.

	Arguments:

		InputBuffer - A buffer containing input data, can be NULL if there
			is no input data.

		InputBufferSize - The size in bytes of the InputBuffer.

		OutputBuffer - A buffer provided by the application that originated
			the communication in which to store data to be returned to the
			application.

		OutputBufferSize - The size in bytes of the OutputBuffer.

		ReturnOutputBufferSize - The size in bytes of meaningful data
			returned in the OutputBuffer.

	Return Value:

		Returns the status of processing the message.

--*/
{
	UNREFERENCED_PARAMETER(PortCookie);
	UNREFERENCED_PARAMETER(InputBufferLength);
	UNREFERENCED_PARAMETER(OutputBufferLength);

	RCV_MSG revMsg;
	UNICODE_STRING fileExtension;
	BOOLEAN success;

	if (InputBuffer == NULL)
		return STATUS_INVALID_PARAMETER;

	InitRCVStruct(&revMsg);

	wcsncpy(revMsg.FileExtension, ((PRCV_MSG)InputBuffer)->FileExtension, 30);
	revMsg.CommandMessage = ((PRCV_MSG)InputBuffer)->CommandMessage;

	switch (revMsg.CommandMessage)
	{
	case ADD_FILE_EXTENSION:

		RtlInitUnicodeString(&fileExtension, revMsg.FileExtension);
		success = AddFilteringFileExtension(&fileExtension);
		(*(PBOOLEAN)OutputBuffer) = success;
		*ReturnOutputBufferLength = sizeof(BOOLEAN);
		break;

	case REMOVE_FILE_EXTENSION:
		RtlInitUnicodeString(&fileExtension, revMsg.FileExtension);
		success = RemoveFilteringFileExtension(&fileExtension);
		(*(PBOOLEAN)OutputBuffer) = success;
		*ReturnOutputBufferLength = sizeof(BOOLEAN);
		break;

	case ENABLE_PROCESS_PROTECTION:

		break;

	case DISABLE_PROCESS_PROTECTON:

		break;

	default:
		break;
	}

	return STATUS_SUCCESS;
}