/*++

Module Name:

	FilterData.h

Abstract:

	This is the filtering data module of the miniFilter driver.

Environment:

	Kernel mode

--*/

#pragma once

#include <fltKernel.h>
#include <ntstrsafe.h>

#include "String.h"

typedef enum _Command_ {

	ADD_FILE_EXTENSION,

	REMOVE_FILE_EXTENSION,

	ENABLE_PROCESS_PROTECTION,

	DISABLE_PROCESS_PROTECTON

} Command;

/*
	Data Communication with User mode App
*/
typedef struct _SEND_MSG_ {

	CHAR ProcessName[40];

	ULONG ProcessId;

	WCHAR FileExtension[30];

	WCHAR FilePath[200];

}SEND_MSG, *PSEND_MSG;

typedef struct _REPLY_MSG_ {

	BOOLEAN IsApproved;

}REPLY_MSG, *PREPLY_MSG;

typedef struct _RECIVED_MSG_ {

	Command CommandMessage;

	WCHAR FileExtension[30];

}RCV_MSG, *PRCV_MSG;



/*************************************************************************
	Prototypes
*************************************************************************/

BOOLEAN
IsTrustedSystemProcess(
	_In_ LPCSTR Requestor
);

BOOLEAN
AddFilteringFileExtension(
	_In_ PUNICODE_STRING FileExtension
);

BOOLEAN
RemoveFilteringFileExtension(
	_In_ PUNICODE_STRING FileExtension
);

INT
IsFilteringFileExtension(
	_In_ PUNICODE_STRING FileExtension
);

VOID
InitMSGStruct(
	_In_ PSEND_MSG Msg
);

VOID
InitRPYStruct(
	_In_ PREPLY_MSG Msg
);

VOID
InitRCVStruct(
	_In_ PRCV_MSG Msg
);