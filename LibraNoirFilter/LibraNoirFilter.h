/*++

Module Name:

	LibraNoirFilter.h

Abstract:

	This is the main module of the LibraNoirFilter miniFilter driver.

Environment:

	Kernel mode

--*/

#pragma once

#include <fltKernel.h>
#include <ntstrsafe.h>

#include "FileInformation.h"
#include "String.h"
#include "FilterData.h"

#define LIBRA_NOIR_FILTER_COMMUNICATION_PORT_NAME L"\\Device\\LibraNoirFilter"

/* Filter Data Handle */
typedef struct _LIBRANOIRFILTER_DATA_ {

	/*
		The object identifies this driver.
	*/
	PDRIVER_OBJECT DriverObject;

	/*
		filter handle
	*/
	PFLT_FILTER Filter;

	/*
		Listens for incoming connections
	*/
	PFLT_PORT ServerPort;

	/*
		User process that connected to the port
	*/
	PEPROCESS UserProcess;

	/*
		Client port for a connection to user-mode
	*/
	PFLT_PORT ClientPort;

}LIBRANOIR_DATA, *PLIBRANOIR_DATA;

//
//  Request Data Structure
//
typedef struct _REQUEST_DATA_ {

	/*
		Requestor Process ID
	*/
	ULONG RequestorPid;

	/*
		Requestor Process Name
	*/
	LPSTR RequestorProcessName;

	/*
		Requested File Name with Full Path
	*/
	UNICODE_STRING FilePath;

	/*
		Requested File Extension
	*/
	UNICODE_STRING FileExtension;

}REQUEST_DATA, *PREQUEST_DATA;

/*************************************************************************
	Prototypes
*************************************************************************/

EXTERN_C_START

DRIVER_INITIALIZE DriverEntry;
NTSTATUS
DriverEntry(
	_In_ PDRIVER_OBJECT DriverObject,
	_In_ PUNICODE_STRING RegistryPath
);

NTSTATUS
LibraNoirFilterInstanceSetup(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_SETUP_FLAGS Flags,
	_In_ DEVICE_TYPE VolumeDeviceType,
	_In_ FLT_FILESYSTEM_TYPE VolumeFilesystemType
);

NTSTATUS
LibraNoirFilterUnload(
	_In_ FLT_FILTER_UNLOAD_FLAGS Flags
);

NTSTATUS
LibraNoirFilterInstanceQueryTeardown(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_QUERY_TEARDOWN_FLAGS Flags
);

FLT_PREOP_CALLBACK_STATUS
LibraNoirFilterPreOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
);

NTSTATUS
LibraNoirFilterPortConnect(
	_In_ PFLT_PORT ClientPort,
	_In_opt_ PVOID ServerPortCookie,
	_In_reads_bytes_opt_(SizeOfContext) PVOID ConnectionContext,
	_In_ ULONG SizeOfContext,
	_Outptr_result_maybenull_ PVOID *ConnectionCookie
);

VOID
LibraNoirFilterPortDisconnect(
	_In_opt_ PVOID ConnectionCookie
);

NTSTATUS
LibraNoirFilterMessage(
	_In_ PVOID PortCookie,
	_In_opt_ PVOID InputBuffer,
	_In_ ULONG InputBufferLength,
	_Out_opt_ PVOID OutputBuffer,
	_In_ ULONG OutputBufferLength,
	_Out_ PULONG ReturnOutputBufferLength
);

EXTERN_C_END

//
//  Assign text sections for each routine.
//

#ifdef ALLOC_PRAGMA
#pragma alloc_text(INIT, DriverEntry)
#pragma alloc_text(PAGE, LibraNoirFilterUnload)
#pragma alloc_text(PAGE, LibraNoirFilterInstanceQueryTeardown)
#pragma alloc_text(PAGE, LibraNoirFilterInstanceSetup)
#pragma alloc_text(PAGE, LibraNoirFilterPortConnect)
#pragma alloc_text(PAGE, LibraNoirFilterPortDisconnect)
#endif

//
//  operation registration
//

CONST FLT_OPERATION_REGISTRATION Callbacks[] = {
	{ IRP_MJ_CREATE,          0, LibraNoirFilterPreOperation, NULL },
	{ IRP_MJ_SET_INFORMATION, 0, LibraNoirFilterPreOperation, NULL },
	{ IRP_MJ_OPERATION_END }
};

//
//  This defines what we want to filter with FltMgr
//

CONST FLT_REGISTRATION FilterRegistration = {

	sizeof(FLT_REGISTRATION),         	//  Size
	FLT_REGISTRATION_VERSION,           //  Version
	0,                                  //  Flags

	NULL,                               //  Context
	Callbacks,                          //  Operation callbacks

	LibraNoirFilterUnload,                           //  MiniFilterUnload

	LibraNoirFilterInstanceSetup,                    //  InstanceSetup
	LibraNoirFilterInstanceQueryTeardown,            //  InstanceQueryTeardown
	NULL,											 //  InstanceTeardownStart
	NULL,											 //  InstanceTeardownComplete

	NULL,                               //  GenerateFileName
	NULL,                               //  GenerateDestinationFileName
	NULL                                //  NormalizeNameComponent

};