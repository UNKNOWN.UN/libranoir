﻿namespace LibraNoir
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.FileExtensionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingButton = new System.Windows.Forms.Button();
            this.fileExtensionButton = new System.Windows.Forms.Button();
            this.Status = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.FolderLockButton = new System.Windows.Forms.Button();
            this.AutoBackupButton = new System.Windows.Forms.Button();
            this.FirewallButton = new System.Windows.Forms.Button();
            this.FolderLockLabel = new System.Windows.Forms.Label();
            this.AutoBackupLabel = new System.Windows.Forms.Label();
            this.FirewallLabel = new System.Windows.Forms.Label();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lockFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fireWallToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "LibraNoir";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.utilityToolStripMenuItem,
            this.FileExtensionToolStripMenuItem,
            this.preferencesToolStripMenuItem,
            this.ExitToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(181, 114);
            // 
            // FileExtensionToolStripMenuItem
            // 
            this.FileExtensionToolStripMenuItem.Name = "FileExtensionToolStripMenuItem";
            this.FileExtensionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.FileExtensionToolStripMenuItem.Text = "File Extension";
            this.FileExtensionToolStripMenuItem.Click += new System.EventHandler(this.FileExtensionToolStripMenuItem_Click);
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ExitToolStripMenuItem.Text = "Exit";
            this.ExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // settingButton
            // 
            this.settingButton.BackColor = System.Drawing.Color.Transparent;
            this.settingButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.settingButton.Image = ((System.Drawing.Image)(resources.GetObject("settingButton.Image")));
            this.settingButton.Location = new System.Drawing.Point(743, 12);
            this.settingButton.Name = "settingButton";
            this.settingButton.Size = new System.Drawing.Size(45, 45);
            this.settingButton.TabIndex = 1;
            this.settingButton.UseVisualStyleBackColor = false;
            this.settingButton.Click += new System.EventHandler(this.settingButton_Click);
            // 
            // fileExtensionButton
            // 
            this.fileExtensionButton.Image = ((System.Drawing.Image)(resources.GetObject("fileExtensionButton.Image")));
            this.fileExtensionButton.Location = new System.Drawing.Point(692, 12);
            this.fileExtensionButton.Name = "fileExtensionButton";
            this.fileExtensionButton.Size = new System.Drawing.Size(45, 45);
            this.fileExtensionButton.TabIndex = 2;
            this.fileExtensionButton.UseVisualStyleBackColor = true;
            this.fileExtensionButton.Click += new System.EventHandler(this.fileExtensionButton_Click);
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Location = new System.Drawing.Point(121, 24);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(62, 12);
            this.Status.TabIndex = 3;
            this.Status.Text = "Not Active";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(12, 24);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(107, 12);
            this.StatusLabel.TabIndex = 4;
            this.StatusLabel.Text = "LibraNoir Status : ";
            // 
            // FolderLockButton
            // 
            this.FolderLockButton.Image = ((System.Drawing.Image)(resources.GetObject("FolderLockButton.Image")));
            this.FolderLockButton.Location = new System.Drawing.Point(26, 84);
            this.FolderLockButton.Name = "FolderLockButton";
            this.FolderLockButton.Size = new System.Drawing.Size(220, 220);
            this.FolderLockButton.TabIndex = 6;
            this.FolderLockButton.UseVisualStyleBackColor = true;
            this.FolderLockButton.Click += new System.EventHandler(this.FolderLockButton_Click);
            // 
            // AutoBackupButton
            // 
            this.AutoBackupButton.Image = ((System.Drawing.Image)(resources.GetObject("AutoBackupButton.Image")));
            this.AutoBackupButton.Location = new System.Drawing.Point(292, 84);
            this.AutoBackupButton.Name = "AutoBackupButton";
            this.AutoBackupButton.Size = new System.Drawing.Size(220, 220);
            this.AutoBackupButton.TabIndex = 7;
            this.AutoBackupButton.UseVisualStyleBackColor = true;
            // 
            // FirewallButton
            // 
            this.FirewallButton.Image = ((System.Drawing.Image)(resources.GetObject("FirewallButton.Image")));
            this.FirewallButton.Location = new System.Drawing.Point(553, 84);
            this.FirewallButton.Name = "FirewallButton";
            this.FirewallButton.Size = new System.Drawing.Size(220, 220);
            this.FirewallButton.TabIndex = 8;
            this.FirewallButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.FirewallButton.UseVisualStyleBackColor = true;
            this.FirewallButton.Click += new System.EventHandler(this.FirewallButton_Click);
            // 
            // FolderLockLabel
            // 
            this.FolderLockLabel.AutoSize = true;
            this.FolderLockLabel.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.FolderLockLabel.Location = new System.Drawing.Point(82, 319);
            this.FolderLockLabel.Name = "FolderLockLabel";
            this.FolderLockLabel.Size = new System.Drawing.Size(105, 16);
            this.FolderLockLabel.TabIndex = 9;
            this.FolderLockLabel.Text = "Lock Folder";
            // 
            // AutoBackupLabel
            // 
            this.AutoBackupLabel.AutoSize = true;
            this.AutoBackupLabel.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.AutoBackupLabel.Location = new System.Drawing.Point(347, 319);
            this.AutoBackupLabel.Name = "AutoBackupLabel";
            this.AutoBackupLabel.Size = new System.Drawing.Size(112, 16);
            this.AutoBackupLabel.TabIndex = 10;
            this.AutoBackupLabel.Text = "Auto Backup";
            // 
            // FirewallLabel
            // 
            this.FirewallLabel.AutoSize = true;
            this.FirewallLabel.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.FirewallLabel.Location = new System.Drawing.Point(631, 319);
            this.FirewallLabel.Name = "FirewallLabel";
            this.FirewallLabel.Size = new System.Drawing.Size(67, 16);
            this.FirewallLabel.TabIndex = 11;
            this.FirewallLabel.Text = "Firewall";
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.preferencesToolStripMenuItem.Text = "Preferences";
            this.preferencesToolStripMenuItem.Click += new System.EventHandler(this.preferencesToolStripMenuItem_Click);
            // 
            // utilityToolStripMenuItem
            // 
            this.utilityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lockFolderToolStripMenuItem,
            this.fireWallToolStripMenuItem});
            this.utilityToolStripMenuItem.Name = "utilityToolStripMenuItem";
            this.utilityToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.utilityToolStripMenuItem.Text = "Utility";
            // 
            // lockFolderToolStripMenuItem
            // 
            this.lockFolderToolStripMenuItem.Name = "lockFolderToolStripMenuItem";
            this.lockFolderToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.lockFolderToolStripMenuItem.Text = "Lock Folder";
            this.lockFolderToolStripMenuItem.Click += new System.EventHandler(this.lockFolderToolStripMenuItem_Click);
            // 
            // fireWallToolStripMenuItem
            // 
            this.fireWallToolStripMenuItem.Name = "fireWallToolStripMenuItem";
            this.fireWallToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.fireWallToolStripMenuItem.Text = "FireWall";
            this.fireWallToolStripMenuItem.Click += new System.EventHandler(this.fireWallToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 371);
            this.Controls.Add(this.FirewallLabel);
            this.Controls.Add(this.AutoBackupLabel);
            this.Controls.Add(this.FolderLockLabel);
            this.Controls.Add(this.FirewallButton);
            this.Controls.Add(this.AutoBackupButton);
            this.Controls.Add(this.FolderLockButton);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.fileExtensionButton);
            this.Controls.Add(this.settingButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "LibraNoir";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_Closing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem FileExtensionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        private System.Windows.Forms.Button settingButton;
        private System.Windows.Forms.Button fileExtensionButton;
        private System.Windows.Forms.Label Status;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Button FolderLockButton;
        private System.Windows.Forms.Button AutoBackupButton;
        private System.Windows.Forms.Button FirewallButton;
        private System.Windows.Forms.Label FolderLockLabel;
        private System.Windows.Forms.Label AutoBackupLabel;
        private System.Windows.Forms.Label FirewallLabel;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utilityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lockFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fireWallToolStripMenuItem;
    }
}