﻿namespace LibraNoir
{
    partial class Setting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Setting));
            this.WarningLevelLabel = new System.Windows.Forms.Label();
            this.FilterLevelcomboBox = new System.Windows.Forms.ComboBox();
            this.SettingSetButton = new System.Windows.Forms.Button();
            this.FilterGroupBox = new System.Windows.Forms.GroupBox();
            this.SuspendLayout();
            // 
            // WarningLevelLabel
            // 
            this.WarningLevelLabel.AutoSize = true;
            this.WarningLevelLabel.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.WarningLevelLabel.Location = new System.Drawing.Point(21, 43);
            this.WarningLevelLabel.Name = "WarningLevelLabel";
            this.WarningLevelLabel.Size = new System.Drawing.Size(143, 12);
            this.WarningLevelLabel.TabIndex = 0;
            this.WarningLevelLabel.Text = "Select Warning Level";
            // 
            // FilterLevelcomboBox
            // 
            this.FilterLevelcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FilterLevelcomboBox.FormattingEnabled = true;
            this.FilterLevelcomboBox.Items.AddRange(new object[] {
            "Using Cloud Data",
            "Trust Digital Signed Process(Default)",
            "Show All Warning"});
            this.FilterLevelcomboBox.Location = new System.Drawing.Point(23, 60);
            this.FilterLevelcomboBox.Name = "FilterLevelcomboBox";
            this.FilterLevelcomboBox.Size = new System.Drawing.Size(236, 20);
            this.FilterLevelcomboBox.TabIndex = 1;
            // 
            // SettingSetButton
            // 
            this.SettingSetButton.Location = new System.Drawing.Point(175, 120);
            this.SettingSetButton.Name = "SettingSetButton";
            this.SettingSetButton.Size = new System.Drawing.Size(96, 23);
            this.SettingSetButton.TabIndex = 2;
            this.SettingSetButton.Text = "Apply/Exit";
            this.SettingSetButton.UseVisualStyleBackColor = true;
            this.SettingSetButton.Click += new System.EventHandler(this.SettingSetButton_Click);
            // 
            // FilterGroupBox
            // 
            this.FilterGroupBox.Location = new System.Drawing.Point(13, 12);
            this.FilterGroupBox.Name = "FilterGroupBox";
            this.FilterGroupBox.Size = new System.Drawing.Size(258, 90);
            this.FilterGroupBox.TabIndex = 3;
            this.FilterGroupBox.TabStop = false;
            this.FilterGroupBox.Text = "Filter";
            // 
            // Setting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(283, 155);
            this.Controls.Add(this.SettingSetButton);
            this.Controls.Add(this.FilterLevelcomboBox);
            this.Controls.Add(this.WarningLevelLabel);
            this.Controls.Add(this.FilterGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Setting";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Preferences";
            this.Load += new System.EventHandler(this.Setting_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label WarningLevelLabel;
        private System.Windows.Forms.ComboBox FilterLevelcomboBox;
        private System.Windows.Forms.Button SettingSetButton;
        private System.Windows.Forms.GroupBox FilterGroupBox;
    }
}