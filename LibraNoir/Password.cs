﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraNoir
{
    public partial class Password : Form
    {
        private string folderPath;
        private bool unlock;
        private Dictionary<string, string> lockedFolderList;
        private FolderLock parent;

        public Password(string _folderPath, bool _unlock, Dictionary<string, string> _lockedFolderList, FolderLock _parent)
        {
            InitializeComponent();

            folderPath = _folderPath;
            unlock = _unlock;
            lockedFolderList = _lockedFolderList;
            parent = _parent;

            if (unlock)
            {
                LockButton.Text = "Unlock";
            }
            else
            {
                LockButton.Text = "Lock";
            }
        }

        private void Password_Load(object sender, EventArgs e)
        {

        }

        private void LockButton_Click(object sender, EventArgs e)
        {
            string hashString = "";

            if (PasswordTextBox.Text != PasswordConfirmTextBox.Text)
            {
                MessageBox.Show("Password doesn't match with confirm", "LibraNoir Warning", MessageBoxButtons.OK);
                PasswordTextBox.Clear();
                PasswordConfirmTextBox.Clear();
            }
            else if (unlock == false) // lock operation
            {
                hashString = getSHA256Hash(PasswordTextBox.Text);

                parent.LockFolder(folderPath);
                lockedFolderList.Add(folderPath, hashString);
                parent.AddFolderinListView(folderPath);

                PasswordTextBox.Clear();
                PasswordConfirmTextBox.Clear();
                MessageBox.Show("Folder Lock Success", "LibraNoir", MessageBoxButtons.OK);
                this.Close();
            }
            else if (unlock == true) // unlock operation
            {
                hashString = getSHA256Hash(PasswordTextBox.Text);
                string result;

                lockedFolderList.TryGetValue(folderPath, out result);
                
                //succuess
                if (result == hashString)
                {
                    parent.UnLockFolder(folderPath);
                    lockedFolderList.Remove(folderPath);
                    parent.RemoveFolderinListView(folderPath);
                    MessageBox.Show("Folder Unlock Success", "LibraNoir", MessageBoxButtons.OK);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Wrong Password", "LibraNoir Warning", MessageBoxButtons.OK);
                    PasswordTextBox.Clear();
                    PasswordConfirmTextBox.Clear();
                }
            }
        }

        private string getSHA256Hash(string input)
        {
            string output = "";

            byte[] bytes = Encoding.Unicode.GetBytes(input);
            SHA256Managed hash = new SHA256Managed();
            byte[] digest = hash.ComputeHash(bytes);

            foreach (byte x in digest)
            {
                output += String.Format("{0:x2}", x);
            }

            return output;
        }
    }
}
