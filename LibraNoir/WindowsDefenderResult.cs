﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace LibraNoir
{
    public class WindowsDefenderResult
    {
        public string Output { get; }
        public Result ResultCode { get; }
        
        public WindowsDefenderResult(string filepath)
        {
            Process p = new Process();

            try
            {
                p.StartInfo.FileName = "\"C:\\Program Files\\Windows Defender\\MpCmdRun.exe\"";
                p.StartInfo.Arguments = "-scan -scantype 3 -file \"" + filepath + "\" -disableremediation";
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.UseShellExecute = false;
                p.Start();

                Output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();

                if (p.ExitCode == 0)
                    ResultCode = Result.OK;
                else if (p.ExitCode == 2)
                    ResultCode = Result.INFECTED;
                else
                    ResultCode = Result.UNKNOWN;
            }
            catch (Exception e)
            {
                Output = String.Empty;
                ResultCode = Result.DEFENDER_NOT_FOUND;
            }
        }

        public enum Result
        {
            OK = 0,
            INFECTED = 2,
            DEFENDER_NOT_FOUND = -1,
            UNKNOWN = -2
        }
    }
}
