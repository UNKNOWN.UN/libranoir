﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LibraNoir
{
    public partial class UserCheck : Form
    {
        private LibraNoirDll.MessageWrap msg;
        private ProcessInfo processInfo;
        private FileInfo fileInfo;
        private LibraNoirDll.LibraNoirFilterManagerWrap manager;
        private ConcurrentQueue<LibraNoirDll.MessageWrap> channel;
        private ConcurrentBag<ProcessInfo> whiteList;
        private ConcurrentBag<ProcessInfo> blackList;

        public UserCheck(LibraNoirDll.MessageWrap _msg, 
            LibraNoirDll.LibraNoirFilterManagerWrap _manager, 
            ProcessInfo _processinfo, FileInfo _fileInfo, 
            ConcurrentQueue<LibraNoirDll.MessageWrap> _channel,
            ConcurrentBag<ProcessInfo> _whiteList,
            ConcurrentBag<ProcessInfo> _blackList)
        {
            InitializeComponent();

            msg = _msg;
            manager = _manager;
            processInfo = _processinfo;
            fileInfo = _fileInfo;
            channel = _channel;
            whiteList = _whiteList;
            blackList = _blackList;

            InitFileListView();
            InitProcessListView();

        }

        private void InitFileListView()
        {
            FileInfoListView.BeginUpdate();

            ListViewItem fileNameItem = new ListViewItem("File Name");
            fileNameItem.SubItems.Add(fileInfo.Name);
            FileInfoListView.Items.Add(fileNameItem);

            ListViewItem filePathItem = new ListViewItem("Path");
            filePathItem.SubItems.Add(fileInfo.FullName);
            FileInfoListView.Items.Add(filePathItem);

            ListViewItem fileLengthItem = new ListViewItem("Length");
            fileLengthItem.SubItems.Add(fileInfo.Length.ToString());
            FileInfoListView.Items.Add(fileLengthItem);

            ListViewItem fileLastWriteItem = new ListViewItem("Last Write Time");
            fileLastWriteItem.SubItems.Add(fileInfo.LastWriteTime.ToString());
            FileInfoListView.Items.Add(fileLastWriteItem);

            FileInfoListView.EndUpdate();
        }

        private void InitProcessListView()
        {
            ProcessInfoListView.BeginUpdate();

            ListViewItem ProcName = new ListViewItem("Process Name");
            ProcName.SubItems.Add(processInfo.ProcessName);
            ProcessInfoListView.Items.Add(ProcName);

            ListViewItem ProcPath = new ListViewItem("Path");
            ProcPath.SubItems.Add(processInfo.ProcessPath);
            ProcessInfoListView.Items.Add(ProcPath);

            ListViewItem Defender = new ListViewItem("Windows Defender");

            WindowsDefenderResult defenderResult = new WindowsDefenderResult(processInfo.ProcessPath);

            if (defenderResult.ResultCode == WindowsDefenderResult.Result.OK)
            {
                ProcPath.SubItems.Add("Found no threats");
                Defender.ForeColor = Color.Green;
            }
            else if (defenderResult.ResultCode == WindowsDefenderResult.Result.INFECTED)
            {
                ProcPath.SubItems.Add("Found threats (Be careful!)");
                Defender.ForeColor = Color.Red;
            }
            else
            {
                ProcPath.SubItems.Add("No Defender Information");
            }
            ProcessInfoListView.Items.Add(ProcPath);

            if (processInfo.CertificateSubject.Equals(String.Empty)) // No Digital Signning
            {
                ListViewItem ProcSignSubject = new ListViewItem("Digital Sign");
                ProcSignSubject.SubItems.Add("No Digital Sign Information (Be Careful!)");
                ProcSignSubject.ForeColor = Color.Red;
                ProcessInfoListView.Items.Add(ProcSignSubject);
            }
            else
            {
                ListViewItem ProcSignSubject = new ListViewItem("Digital Sign");
                ProcSignSubject.SubItems.Add(processInfo.CertificateSubject);
                ProcSignSubject.ForeColor = Color.Green;
                ProcessInfoListView.Items.Add(ProcSignSubject);

                ListViewItem ProcSignIssuer = new ListViewItem("Issuer");
                ProcSignIssuer.SubItems.Add(processInfo.CertificateIssuer);
                ProcessInfoListView.Items.Add(ProcSignIssuer);
            }

            ProcessInfoListView.EndUpdate();
        }

        private void UserCheck_Load(object sender, EventArgs e)
        {

        }

        private void WatchButton_Click(object sender, EventArgs e)
        {

        }

        private void PermitButton_Click(object sender, EventArgs e)
        {
            manager.ReplyMessageWrap(msg._msgId, true);
            //Add to white list
            whiteList.Add(processInfo);
            while(channel.Count > 0)
            {
                LibraNoirDll.MessageWrap msg;
                channel.TryDequeue(out msg);
                manager.ReplyMessageWrap(msg._msgId, true);
            }
            Application.Exit();
        }

        private void DenyButton_Click(object sender, EventArgs e)
        {
            manager.ReplyMessageWrap(msg._msgId, false);
            //Add to Black List
            blackList.Add(processInfo);
            while (channel.Count > 0)
            {
                LibraNoirDll.MessageWrap msg;
                channel.TryDequeue(out msg);
                manager.ReplyMessageWrap(msg._msgId, false);
            }
            Application.Exit();
        }

        private void LinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://google.com/search?q=\"" + processInfo.ProcessName + "\"");
        }
    }
}
