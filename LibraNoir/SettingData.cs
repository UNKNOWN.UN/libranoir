﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraNoir
{
    public class SettingData
    {
        public FilterLevel CurrentFilterLevel { get; set; }

        public SettingData()
        {
            
        }

        public enum FilterLevel
        {
            TrustCloudData,
            TrustSignedFile,
            Normal
        }
    }
}
