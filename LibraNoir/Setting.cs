﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraNoir
{
    public partial class Setting : Form
    {
        private SettingData data;

        public Setting(SettingData _data)
        {
            InitializeComponent();

            this.data = _data;
            
            switch (data.CurrentFilterLevel)
            {
                case SettingData.FilterLevel.TrustCloudData:
                    FilterLevelcomboBox.SelectedIndex = 0;

                    break;
                case SettingData.FilterLevel.TrustSignedFile:
                    FilterLevelcomboBox.SelectedIndex = 1;

                    break;
                case SettingData.FilterLevel.Normal:
                    FilterLevelcomboBox.SelectedIndex = 2;

                    break;
            }
        }

        private void Setting_Load(object sender, EventArgs e)
        {

        }

        private void SettingSetButton_Click(object sender, EventArgs e)
        {
            switch(FilterLevelcomboBox.SelectedIndex)
            {
                case 0:
                    data.CurrentFilterLevel = SettingData.FilterLevel.TrustCloudData;
                    break;
                case 1:
                    data.CurrentFilterLevel = SettingData.FilterLevel.TrustSignedFile;
                    break;
                case 2:
                    data.CurrentFilterLevel = SettingData.FilterLevel.Normal;
                    break;
            }

            this.Close();
        }
    }
}
