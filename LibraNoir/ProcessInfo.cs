﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;


namespace LibraNoir
{
    public class ProcessInfo : IEquatable<ProcessInfo>, IEqualityComparer<ProcessInfo>
    {
        public uint Pid { get; }
        public string ProcessName { get; }
        public string ProcessPath { get; }
        public string ProccessSHA512Hash { get; }
        public string CertificateSubject { get; }
        public string CertificateIssuer { get; }
        public bool HasCertificate { get; }

        public ProcessInfo(uint pid)
        {
            this.Pid = pid;
            
            using (Process process = Process.GetProcessById(Convert.ToInt32(pid)))
            {
                this.ProcessName = process.ProcessName;
                this.ProcessPath = process.MainModule.FileName;
            }

            using (FileStream stream = File.OpenRead(ProcessPath))
            {
                SHA256 sHA256 = new SHA256CryptoServiceProvider();
                byte[] byteChecksum = sHA256.ComputeHash(stream);
                this.ProccessSHA512Hash = BitConverter.ToString(byteChecksum).Replace("-", String.Empty);
            }

            try
            {
                X509Certificate theSigner = X509Certificate.CreateFromSignedFile(ProcessPath);
                this.CertificateSubject = theSigner.Subject;
                this.CertificateIssuer = theSigner.Issuer;
                this.HasCertificate = true;
            }
            catch(Exception e)
            {
                this.CertificateSubject = String.Empty;
                this.CertificateIssuer = String.Empty;
                this.HasCertificate = false;
            }
        }

        public bool Equals(ProcessInfo other)
        {
            return this.ProccessSHA512Hash.Equals(other.ProccessSHA512Hash);
        }

        public bool Equals(ProcessInfo x, ProcessInfo y)
        {
            return x.ProccessSHA512Hash.Equals(y.ProccessSHA512Hash);
        }

        public int GetHashCode(ProcessInfo obj)
        {
            return obj.ProccessSHA512Hash.GetHashCode();
        }
    }
}
