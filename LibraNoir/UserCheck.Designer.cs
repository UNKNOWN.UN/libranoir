﻿namespace LibraNoir
{
    partial class UserCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IntroductionText = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FileInfoListView = new System.Windows.Forms.ListView();
            this.List = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Contents = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.ProcessInfoListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DenyButton = new System.Windows.Forms.Button();
            this.PermitButton = new System.Windows.Forms.Button();
            this.WatchButton = new System.Windows.Forms.Button();
            this.LinkLabel = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // IntroductionText
            // 
            this.IntroductionText.AutoSize = true;
            this.IntroductionText.Font = new System.Drawing.Font("굴림", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.IntroductionText.ForeColor = System.Drawing.Color.Red;
            this.IntroductionText.Location = new System.Drawing.Point(14, 13);
            this.IntroductionText.Name = "IntroductionText";
            this.IntroductionText.Size = new System.Drawing.Size(386, 45);
            this.IntroductionText.TabIndex = 0;
            this.IntroductionText.Text = "The following program is trying to modify your file.\r\nIf it\'s a well-known progra" +
    "m, click permit.\r\nIf it\'s a suspicious program, click deny.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 10F);
            this.label1.Location = new System.Drawing.Point(14, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "File Information";
            // 
            // FileInfoListView
            // 
            this.FileInfoListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.List,
            this.Contents});
            this.FileInfoListView.Location = new System.Drawing.Point(12, 95);
            this.FileInfoListView.Name = "FileInfoListView";
            this.FileInfoListView.Size = new System.Drawing.Size(430, 155);
            this.FileInfoListView.TabIndex = 2;
            this.FileInfoListView.UseCompatibleStateImageBehavior = false;
            this.FileInfoListView.View = System.Windows.Forms.View.Details;
            // 
            // List
            // 
            this.List.Text = "List";
            this.List.Width = 100;
            // 
            // Contents
            // 
            this.Contents.Text = "Contents";
            this.Contents.Width = 400;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 10F);
            this.label2.Location = new System.Drawing.Point(14, 266);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 14);
            this.label2.TabIndex = 3;
            this.label2.Text = "Process Information";
            // 
            // ProcessInfoListView
            // 
            this.ProcessInfoListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.ProcessInfoListView.Location = new System.Drawing.Point(12, 287);
            this.ProcessInfoListView.Name = "ProcessInfoListView";
            this.ProcessInfoListView.Size = new System.Drawing.Size(430, 235);
            this.ProcessInfoListView.TabIndex = 4;
            this.ProcessInfoListView.UseCompatibleStateImageBehavior = false;
            this.ProcessInfoListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "List";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Contents";
            this.columnHeader2.Width = 400;
            // 
            // DenyButton
            // 
            this.DenyButton.Location = new System.Drawing.Point(349, 557);
            this.DenyButton.Name = "DenyButton";
            this.DenyButton.Size = new System.Drawing.Size(93, 34);
            this.DenyButton.TabIndex = 5;
            this.DenyButton.Text = "Deny";
            this.DenyButton.UseVisualStyleBackColor = true;
            this.DenyButton.Click += new System.EventHandler(this.DenyButton_Click);
            // 
            // PermitButton
            // 
            this.PermitButton.Location = new System.Drawing.Point(245, 557);
            this.PermitButton.Name = "PermitButton";
            this.PermitButton.Size = new System.Drawing.Size(93, 34);
            this.PermitButton.TabIndex = 6;
            this.PermitButton.Text = "Permit";
            this.PermitButton.UseVisualStyleBackColor = true;
            this.PermitButton.Click += new System.EventHandler(this.PermitButton_Click);
            // 
            // WatchButton
            // 
            this.WatchButton.Location = new System.Drawing.Point(16, 557);
            this.WatchButton.Name = "WatchButton";
            this.WatchButton.Size = new System.Drawing.Size(93, 34);
            this.WatchButton.TabIndex = 7;
            this.WatchButton.Text = "Monitor";
            this.WatchButton.UseVisualStyleBackColor = true;
            this.WatchButton.Click += new System.EventHandler(this.WatchButton_Click);
            // 
            // LinkLabel
            // 
            this.LinkLabel.AutoSize = true;
            this.LinkLabel.Location = new System.Drawing.Point(212, 530);
            this.LinkLabel.Name = "LinkLabel";
            this.LinkLabel.Size = new System.Drawing.Size(227, 12);
            this.LinkLabel.TabIndex = 8;
            this.LinkLabel.TabStop = true;
            this.LinkLabel.Text = "Check this process information on web";
            this.LinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel_LinkClicked);
            // 
            // UserCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 604);
            this.ControlBox = false;
            this.Controls.Add(this.LinkLabel);
            this.Controls.Add(this.WatchButton);
            this.Controls.Add(this.PermitButton);
            this.Controls.Add(this.DenyButton);
            this.Controls.Add(this.ProcessInfoListView);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.FileInfoListView);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.IntroductionText);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserCheck";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "LibraNoir Warning";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.UserCheck_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label IntroductionText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView FileInfoListView;
        private System.Windows.Forms.ColumnHeader List;
        private System.Windows.Forms.ColumnHeader Contents;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView ProcessInfoListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button DenyButton;
        private System.Windows.Forms.Button PermitButton;
        private System.Windows.Forms.Button WatchButton;
        private System.Windows.Forms.LinkLabel LinkLabel;
    }
}