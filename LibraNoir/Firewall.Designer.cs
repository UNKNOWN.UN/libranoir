﻿namespace LibraNoir
{
    partial class Firewall
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.netstatListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RefreshButton = new System.Windows.Forms.Button();
            this.DenyButton = new System.Windows.Forms.Button();
            this.AllowButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // netstatListView
            // 
            this.netstatListView.CheckBoxes = true;
            this.netstatListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader5,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.netstatListView.Location = new System.Drawing.Point(12, 15);
            this.netstatListView.Name = "netstatListView";
            this.netstatListView.Size = new System.Drawing.Size(765, 387);
            this.netstatListView.TabIndex = 0;
            this.netstatListView.UseCompatibleStateImageBehavior = false;
            this.netstatListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Process";
            this.columnHeader1.Width = 200;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Local Address";
            this.columnHeader2.Width = 170;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Foreign Address";
            this.columnHeader3.Width = 170;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Status";
            this.columnHeader4.Width = 150;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "PID";
            // 
            // RefreshButton
            // 
            this.RefreshButton.Location = new System.Drawing.Point(12, 415);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(75, 23);
            this.RefreshButton.TabIndex = 1;
            this.RefreshButton.Text = "Refresh";
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // DenyButton
            // 
            this.DenyButton.Location = new System.Drawing.Point(702, 415);
            this.DenyButton.Name = "DenyButton";
            this.DenyButton.Size = new System.Drawing.Size(75, 23);
            this.DenyButton.TabIndex = 2;
            this.DenyButton.Text = "Deny";
            this.DenyButton.UseVisualStyleBackColor = true;
            this.DenyButton.Click += new System.EventHandler(this.DenyButton_Click);
            // 
            // AllowButton
            // 
            this.AllowButton.Location = new System.Drawing.Point(610, 415);
            this.AllowButton.Name = "AllowButton";
            this.AllowButton.Size = new System.Drawing.Size(75, 23);
            this.AllowButton.TabIndex = 3;
            this.AllowButton.Text = "Allow";
            this.AllowButton.UseVisualStyleBackColor = true;
            this.AllowButton.Click += new System.EventHandler(this.AllowButton_Click);
            // 
            // Firewall
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(791, 450);
            this.Controls.Add(this.AllowButton);
            this.Controls.Add(this.DenyButton);
            this.Controls.Add(this.RefreshButton);
            this.Controls.Add(this.netstatListView);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Firewall";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Firewall";
            this.Load += new System.EventHandler(this.Firewall_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView netstatListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.Button DenyButton;
        private System.Windows.Forms.Button AllowButton;
    }
}