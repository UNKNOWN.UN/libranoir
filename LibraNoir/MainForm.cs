﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace LibraNoir
{
    public partial class MainForm : Form
    {
        private Setting settingForm;
        private FileExtension fileExtension;
        private FolderLock FolderLockForm;
        private Firewall firewallForm;
        
        private SettingData settingData;
        private List<string> fileExtensionList = new List<string>();

        private ConcurrentBag<ProcessInfo> whiteList = new ConcurrentBag<ProcessInfo>();
        private ConcurrentBag<ProcessInfo> blackList = new ConcurrentBag<ProcessInfo>();

        private Dictionary<string, string> LockedFolderList = new Dictionary<string, string>();

        private LibraNoirDll.LibraNoirFilterManagerWrap libraNoirFilterManager;

        private Thread FilterListenThread;

        public MainForm()
        {
            InitializeComponent();

            
            this.notifyIcon.ContextMenuStrip = contextMenuStrip;

            this.notifyIcon.Visible = true;

            this.MaximizeBox = false;
            
            settingData = new SettingData
            {
                CurrentFilterLevel = SettingData.FilterLevel.TrustSignedFile
            };

            firewallForm = new Firewall()
            {
                Owner = this
            };

            settingForm = new Setting(settingData)
            {
                Owner = this
            };
            
            Status.Text = "Not Active";

            libraNoirFilterManager = new LibraNoirDll.LibraNoirFilterManagerWrap();
            if (libraNoirFilterManager.isConnectedWrap())
            {
                Status.Text = "Active";

                FilterListenThread = new Thread(new ThreadStart(FilterRun));
                FilterListenThread.IsBackground = true;
                FilterListenThread.Start();
            }

            fileExtension = new FileExtension(fileExtensionList, libraNoirFilterManager)
            {
                Owner = this
            };

            FolderLockForm = new FolderLock(LockedFolderList)
            {
                Owner = this
            };
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Visible = true;
            if (this.WindowState == FormWindowState.Minimized)
                this.WindowState = FormWindowState.Normal;
            this.Activate();
            
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Really Terminate LibraNoir?", "LibraNoir Exit",MessageBoxButtons.YesNo)
                == DialogResult.Yes)
            {
                notifyIcon.Visible = false;
                this.Dispose();
                Application.Exit();
            }

        }

        private void MainForm_Closing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.ToTray();
        }

        private void ToTray()
        {
            this.Visible = false;
            this.ShowInTaskbar = false;
        }

        private void settingButton_Click(object sender, EventArgs e)
        {
            settingForm.ShowDialog();
        }

        private void fileExtensionButton_Click(object sender, EventArgs e)
        {
            fileExtension.ShowDialog();
        }
        
        private void FilterRun()
        {
            // NEW THREAD
            LibraNoirDll.MessageWrap message;
            var channels = new ConcurrentDictionary<ProcessInfo, ConcurrentQueue<LibraNoirDll.MessageWrap>>();
            ProcessInfo processinfo;
            FileInfo fileInfo;

            while (true)
            {
                /*
                 * TODO: MSG LOGIC
                 * 
                 * PROCESS is in White list => approved automatically
                 * PROCESS is in Black list => denyed automatically
                 * 
                 * Neither, User decide what to do
                 * 1. Create Thread
                 *   - param message(data from filter)
                 *   - filtercommunicationmanager instance
                 *   - channel set (msg queue between thread) - maybe ConcurrentDictionary<string=proc name, concurrentQueue=channel> instance
                 * 
                 * 2. The Thread make form to get decision from user and send to filter driver.
                 *   - permit
                 *   - deny
                 * 
                 * ***** NOTICE *****
                 * MUST ADD LOGIC : PREFERENCE
                 * 
                 */

                message = libraNoirFilterManager.WaitMessagWrap();

                processinfo = new ProcessInfo(message._pid);
                fileInfo = new FileInfo(message._filepath);

                if (whiteList.Contains(processinfo))
                {
                    libraNoirFilterManager.ReplyMessageWrap(message._msgId, true);
                }
                else if (blackList.Contains(processinfo))
                {
                    libraNoirFilterManager.ReplyMessageWrap(message._msgId, false);
                }
                else
                {
                    ConcurrentQueue<LibraNoirDll.MessageWrap> channel;

                    // check channel(queue) is already make(use dictionary)
                    if (channels.TryGetValue(processinfo, out channel) == true)
                    {
                        channel.Enqueue(message);
                    }
                    else // or make queue and thread to user check
                    {
                        channel = new ConcurrentQueue<LibraNoirDll.MessageWrap>();
                        channels.TryAdd(processinfo, channel);

                        Thread userCheckThread = new Thread(() => UserCheckRun(message, processinfo, fileInfo, channels));
                        userCheckThread.Start();
                    }
                }
                
            }
        }

        private void UserCheckRun(LibraNoirDll.MessageWrap msg, 
            ProcessInfo processinfo, FileInfo fileInfo, 
            ConcurrentDictionary<ProcessInfo, ConcurrentQueue<LibraNoirDll.MessageWrap>> channels)
        {
            ConcurrentQueue<LibraNoirDll.MessageWrap> channel;
            channels.TryGetValue(processinfo, out channel);

            UserCheck userCheckForm = new UserCheck(msg, libraNoirFilterManager, processinfo, fileInfo, channel, whiteList, blackList);
            Application.Run(userCheckForm);

            channels.TryRemove(processinfo, out channel);
        }

        private void preferencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            settingForm.Show();
        }

        private void FileExtensionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fileExtension.Show();
        }

        private void FolderLockButton_Click(object sender, EventArgs e)
        {
            FolderLockForm.ShowDialog();
        }

        private void lockFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderLockForm.Show();
        }

        private void FirewallButton_Click(object sender, EventArgs e)
        {
            firewallForm.ShowDialog();
        }

        private void fireWallToolStripMenuItem_Click(object sender, EventArgs e)
        {
            firewallForm.Show();
        }
    }
}
