﻿namespace LibraNoir
{
    partial class FileExtension
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileExtension));
            this.label1 = new System.Windows.Forms.Label();
            this.UserinputTextbox = new System.Windows.Forms.TextBox();
            this.Addbutton = new System.Windows.Forms.Button();
            this.Deletebutton = new System.Windows.Forms.Button();
            this.FileExtensionListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.InvaildLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // UserinputTextbox
            // 
            resources.ApplyResources(this.UserinputTextbox, "UserinputTextbox");
            this.UserinputTextbox.Name = "UserinputTextbox";
            // 
            // Addbutton
            // 
            resources.ApplyResources(this.Addbutton, "Addbutton");
            this.Addbutton.Name = "Addbutton";
            this.Addbutton.UseVisualStyleBackColor = true;
            this.Addbutton.Click += new System.EventHandler(this.Addbutton_Click);
            // 
            // Deletebutton
            // 
            resources.ApplyResources(this.Deletebutton, "Deletebutton");
            this.Deletebutton.Name = "Deletebutton";
            this.Deletebutton.UseVisualStyleBackColor = true;
            this.Deletebutton.Click += new System.EventHandler(this.Deletebutton_Click);
            // 
            // FileExtensionListView
            // 
            this.FileExtensionListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            resources.ApplyResources(this.FileExtensionListView, "FileExtensionListView");
            this.FileExtensionListView.Name = "FileExtensionListView";
            this.FileExtensionListView.UseCompatibleStateImageBehavior = false;
            this.FileExtensionListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            resources.ApplyResources(this.columnHeader1, "columnHeader1");
            // 
            // InvaildLabel
            // 
            resources.ApplyResources(this.InvaildLabel, "InvaildLabel");
            this.InvaildLabel.ForeColor = System.Drawing.Color.Red;
            this.InvaildLabel.Name = "InvaildLabel";
            // 
            // FileExtension
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.InvaildLabel);
            this.Controls.Add(this.FileExtensionListView);
            this.Controls.Add(this.Deletebutton);
            this.Controls.Add(this.Addbutton);
            this.Controls.Add(this.UserinputTextbox);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FileExtension";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.FileExtension_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox UserinputTextbox;
        private System.Windows.Forms.Button Addbutton;
        private System.Windows.Forms.Button Deletebutton;
        private System.Windows.Forms.ListView FileExtensionListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Label InvaildLabel;
    }
}