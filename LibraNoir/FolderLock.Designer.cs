﻿namespace LibraNoir
{
    partial class FolderLock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LockFolderListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.FolderPathTextBox = new System.Windows.Forms.TextBox();
            this.BrowerButton = new System.Windows.Forms.Button();
            this.LockUnlockButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LockFolderListView
            // 
            this.LockFolderListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.LockFolderListView.Location = new System.Drawing.Point(12, 31);
            this.LockFolderListView.Name = "LockFolderListView";
            this.LockFolderListView.Size = new System.Drawing.Size(776, 251);
            this.LockFolderListView.TabIndex = 0;
            this.LockFolderListView.UseCompatibleStateImageBehavior = false;
            this.LockFolderListView.View = System.Windows.Forms.View.Details;
            this.LockFolderListView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FolderListViewMouseClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Path";
            this.columnHeader1.Width = 800;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "Registered Lock Folders";
            // 
            // FolderPathTextBox
            // 
            this.FolderPathTextBox.Location = new System.Drawing.Point(132, 300);
            this.FolderPathTextBox.Name = "FolderPathTextBox";
            this.FolderPathTextBox.Size = new System.Drawing.Size(441, 21);
            this.FolderPathTextBox.TabIndex = 2;
            // 
            // BrowerButton
            // 
            this.BrowerButton.Location = new System.Drawing.Point(589, 300);
            this.BrowerButton.Name = "BrowerButton";
            this.BrowerButton.Size = new System.Drawing.Size(75, 23);
            this.BrowerButton.TabIndex = 3;
            this.BrowerButton.Text = "Browse";
            this.BrowerButton.UseVisualStyleBackColor = true;
            this.BrowerButton.Click += new System.EventHandler(this.BrowerButton_Click);
            // 
            // LockUnlockButton
            // 
            this.LockUnlockButton.Location = new System.Drawing.Point(670, 300);
            this.LockUnlockButton.Name = "LockUnlockButton";
            this.LockUnlockButton.Size = new System.Drawing.Size(118, 23);
            this.LockUnlockButton.TabIndex = 4;
            this.LockUnlockButton.Text = "Lock/Unlock";
            this.LockUnlockButton.UseVisualStyleBackColor = true;
            this.LockUnlockButton.Click += new System.EventHandler(this.LockUnlockButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(12, 301);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Folder Path :";
            // 
            // FolderLock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 337);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LockUnlockButton);
            this.Controls.Add(this.BrowerButton);
            this.Controls.Add(this.FolderPathTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LockFolderListView);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FolderLock";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "FolderLock";
            this.Load += new System.EventHandler(this.FolderLock_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView LockFolderListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FolderPathTextBox;
        private System.Windows.Forms.Button BrowerButton;
        private System.Windows.Forms.Button LockUnlockButton;
        private System.Windows.Forms.Label label2;
    }
}