﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraNoir
{
    public partial class FileExtension : Form
    {
        private List<string> fileExtensionList;
        private LibraNoirDll.LibraNoirFilterManagerWrap manager;

        public FileExtension(List<string> _fileExtensionList, LibraNoirDll.LibraNoirFilterManagerWrap _manager)
        {
            InitializeComponent();

            UserinputTextbox.MaxLength = 8;
            fileExtensionList = _fileExtensionList;
            manager = _manager;
        }

        private void FileExtension_Load(object sender, EventArgs e)
        {

        }

        private void Addbutton_Click(object sender, EventArgs e)
        {
            if (VerifyFileExtension(UserinputTextbox.Text))
            {
                if (fileExtensionList.Contains(UserinputTextbox.Text))
                {
                    InvaildLabel.Text = "Already Exist";
                    return;
                }
                FileExtensionListView.BeginUpdate();
                ListViewItem newfileExtension = new ListViewItem(UserinputTextbox.Text);
                FileExtensionListView.Items.Add(newfileExtension);
                FileExtensionListView.EndUpdate();

                fileExtensionList.Add(UserinputTextbox.Text);
                manager.AddFileExtensionWrap(UserinputTextbox.Text);

                UserinputTextbox.Clear();
                InvaildLabel.Text = "";
                
            }
            else
            {
                InvaildLabel.Text = "Invaild Input [a-Z0-9]";
            }
        }

        private void Deletebutton_Click(object sender, EventArgs e)
        {
            if (VerifyFileExtension(UserinputTextbox.Text))
            {
                //TODO : Remove File extension to List and ListView
                try
                {
                    FileExtensionListView.FindItemWithText(UserinputTextbox.Text).Remove();

                    FileExtensionListView.BeginUpdate();
                    fileExtensionList.Remove(UserinputTextbox.Text);
                    FileExtensionListView.EndUpdate();
                    manager.RemoveFileExtensionWrap(UserinputTextbox.Text);

                    UserinputTextbox.Clear();
                    InvaildLabel.Text = "";
                }
                catch(Exception ex)
                {
                    InvaildLabel.Text = "Input Exist Extension";
                }
                
            }
            else
            {
                InvaildLabel.Text = "Invaild Input [a-Z0-9]";
            }
        }
        
        private bool VerifyFileExtension(string fileExtension)
        {
            if (Regex.IsMatch(fileExtension, "^[a-zA-Z0-9]*$")) return true;
            return false;
        }
    }
}
