﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraNoir
{
    public partial class FolderLock : Form
    {
        private string adminUserName;
        private Dictionary<string, string> lockedFolderList; // folderpath(key) - password(value) hash code

        public FolderLock(Dictionary<string, string> _lockedFolderList)
        {
            InitializeComponent();

            adminUserName = Environment.UserName;
            lockedFolderList = _lockedFolderList;
            
        }

        private void FolderLock_Load(object sender, EventArgs e)
        {

        }

        private void BrowerButton_Click(object sender, EventArgs e)
        {
            var fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
            {
                FolderPathTextBox.Text = fbd.SelectedPath;
            }
        }

        private void LockUnlockButton_Click(object sender, EventArgs e)
        {
            /*
             * 0. check folder exist.
             * 1. check it's lock operation or unlock operation
             * 2. make new form to get password
             * 3. save folder path and password hash code
             */

            bool isUnlock;

            if (Directory.Exists(FolderPathTextBox.Text))
            {
                if (lockedFolderList.ContainsKey(FolderPathTextBox.Text))
                {
                    //Unlock operation
                    isUnlock = true;
                }
                else
                {
                    //lock operation
                    isUnlock = false;
                }

                Password passwordForm = new Password(FolderPathTextBox.Text, isUnlock, lockedFolderList, this)
                {
                    Owner = this
                };
                passwordForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Invaild Folder Path", "LibraNoir Warning", MessageBoxButtons.OK);
            }
        }

        public void AddFolderinListView(string filepath)
        {
            LockFolderListView.BeginUpdate();
            ListViewItem item = new ListViewItem(filepath);
            LockFolderListView.Items.Add(item);

            LockFolderListView.EndUpdate();
        }

        public void RemoveFolderinListView(string filepath)
        {
            LockFolderListView.BeginUpdate();
            try
            {
                LockFolderListView.FindItemWithText(filepath).Remove();
            }
            catch(Exception ex)
            {

            }

            LockFolderListView.EndUpdate();
        }

        public bool LockFolder(string folderPath)
        {
            try
            {
                DirectorySecurity ds = Directory.GetAccessControl(folderPath);
                FileSystemAccessRule fsa = new FileSystemAccessRule(adminUserName, FileSystemRights.FullControl, AccessControlType.Deny);

                ds.AddAccessRule(fsa);
                Directory.SetAccessControl(folderPath, ds);


            }
            catch(Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool UnLockFolder(string folderPath)
        {
            try
            {
                DirectorySecurity ds = Directory.GetAccessControl(folderPath);
                FileSystemAccessRule fsa = new FileSystemAccessRule(adminUserName, FileSystemRights.FullControl, AccessControlType.Deny);

                ds.RemoveAccessRule(fsa);
                Directory.SetAccessControl(folderPath, ds);


            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void FolderListViewMouseClick(object sender, MouseEventArgs e)
        {
            FolderPathTextBox.Text = LockFolderListView.GetItemAt(e.X, e.Y).Text;
        }
    }
}
