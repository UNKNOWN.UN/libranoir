﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraNoir
{
    public partial class Firewall : Form
    {
        List<List<string>> netstatResult = new List<List<string>>();
        
        public Firewall()
        {
            InitializeComponent();

            netstatListView.CheckBoxes = true;
        }

        private void Firewall_Load(object sender, EventArgs e)
        {
            netstatResult = GetNetstatInfo();
            UpdateListView();
        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            netstatResult = GetNetstatInfo();
            UpdateListView();
        }

        private void AllowButton_Click(object sender, EventArgs e)
        {

        }

        private void DenyButton_Click(object sender, EventArgs e)
        {

        }

        private void UpdateListView()
        {
            netstatListView.BeginUpdate();

            foreach(var list in netstatResult)
            {
                ListViewItem item = new ListViewItem(list[5]);
                item.SubItems.Add(list[4]);
                item.SubItems.Add(list[1]);
                item.SubItems.Add(list[2]);
                item.SubItems.Add(list[3]);
                netstatListView.Items.Add(item);
            }

            netstatListView.Sorting = SortOrder.Ascending;
            netstatListView.EndUpdate();
        }

        public List<List<string>> GetNetstatInfo()
        {
            char[] vs = { ' ', '\t' };

            List<List<string>> result = new List<List<string>>();

            Process net = new Process();
            net.StartInfo.FileName = "netstat";
            net.StartInfo.Arguments = "-no";
            net.StartInfo.RedirectStandardOutput = true;
            net.StartInfo.RedirectStandardError = true;
            net.StartInfo.UseShellExecute = false;
            net.StartInfo.CreateNoWindow = true;
            net.StartInfo.UseShellExecute = false;
            net.Start();

            string output;
            List<string> outputList;

            while ((output = net.StandardOutput.ReadLine()) != null)
            {
                output = output.Trim();
                outputList = new List<string>();

                string[] splitResult = output.Split(vs, StringSplitOptions.RemoveEmptyEntries);
                
                if (splitResult.GetLength(0) > 0)
                {
                    if (splitResult[0] == "TCP" && splitResult[4] != "0")
                    {
                        foreach (string str in splitResult)
                        {
                            outputList.Add(str);
                        }
                        outputList.Add(Process.GetProcessById(Convert.ToInt32(splitResult[4])).ProcessName);

                        result.Add(outputList);
                    }
                }
            }

            return result;
        }
    }
}
