﻿#pragma once

#pragma comment(lib, "FltLib.lib")

#include "stdafx.h"
#include "Message.h"
#include "Communication.h"
#include "LibraNoirFilterManager.h"

using namespace System;

namespace LibraNoirDll {
	public ref class LibraNoirFilterManagerWrap
	{

	public:

		LibraNoirFilterManagerWrap();
		~LibraNoirFilterManagerWrap();

		bool InstallProcessProctectionWrap();
		bool UnInstallProcessProtectionWrap();

		bool AddFileExtensionWrap(System::String^ fileExtension);
		bool RemoveFileExtensionWrap(System::String^ fileExtension);

		ref class MessageWrap^ WaitMessagWrap();
		bool ReplyMessageWrap(unsigned long long msgId, bool permit);

		bool isConnectedWrap();

	private:
		LibraNoir::LibraNoirFilterManager* _manager;
	};

	public ref class MessageWrap
	{

	public:

		MessageWrap(unsigned long pid, wchar_t* filepath, unsigned long long msgId);
		~MessageWrap();

		unsigned long _pid;
		System::String^ _filepath;
		unsigned long long _msgId;
	};
}
