#pragma once

#include "stdafx.h"

#include "Message.h"

namespace LibraNoir {

	constexpr auto COMMUNICATION_PORT_NAME = L"\\Device\\LibraNoirFilter";

	class FilterCommunicationManager {

	public:

		FilterCommunicationManager();
		~FilterCommunicationManager();

		BOOLEAN ConnectToFilter();
		BOOLEAN IsConnected();

		BOOLEAN GetMessageFromFilter(SEND_DATA& data);
		BOOLEAN ReplyMessageToFilter(REPLY_DATA& data);

		BOOLEAN SendMessageToFilter(RCV_MSG& data, BOOLEAN& success);

	private:

		HANDLE _port;
	};
}
