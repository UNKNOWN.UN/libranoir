#include "stdafx.h"

#include "LibraNoirDll.h"

namespace LibraNoirDll {

	LibraNoirFilterManagerWrap::LibraNoirFilterManagerWrap() : _manager(new LibraNoir::LibraNoirFilterManager())
	{

	}

	LibraNoirFilterManagerWrap::~LibraNoirFilterManagerWrap()
	{
		if (_manager)
			delete _manager;
	}

	bool LibraNoirFilterManagerWrap::InstallProcessProctectionWrap()
	{
		return _manager->InstallProcessProctection();
	}

	bool LibraNoirFilterManagerWrap::UnInstallProcessProtectionWrap()
	{
		return _manager->UnInstallProcessProtection();
	}

	bool LibraNoirFilterManagerWrap::AddFileExtensionWrap(System::String^ fileExtension)
	{
		return _manager->AddFileExtension(msclr::interop::marshal_as<std::wstring>(fileExtension));
	}

	bool LibraNoirFilterManagerWrap::RemoveFileExtensionWrap(System::String^ fileExtension)
	{
		return _manager->RemoveFileExtension(msclr::interop::marshal_as<std::wstring>(fileExtension));
	}

	ref class MessageWrap^ LibraNoirFilterManagerWrap::WaitMessagWrap()
	{
		LibraNoir::SEND_DATA data = _manager->WaitMessage();

		ref class MessageWrap^ msg = gcnew MessageWrap(data.Msg.ProcessId, data.Msg.FilePath, data.MessageHeader.MessageId);

		return msg;
	}

	bool LibraNoirFilterManagerWrap::ReplyMessageWrap(unsigned long long msgId, bool permit)
	{
		return _manager->ReplyMessage(msgId, permit);
	}

	bool LibraNoirFilterManagerWrap::isConnectedWrap()
	{
		return _manager->isConnected();
	}

	MessageWrap::MessageWrap(unsigned long pid, wchar_t* filepath, unsigned long long msgId) : _pid(pid), _filepath(gcnew System::String(filepath)), _msgId(msgId)
	{

	}

	MessageWrap::~MessageWrap()
	{

	}
}