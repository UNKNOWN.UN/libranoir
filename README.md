# LibraNoir (KUCIS 2019)

### What is LibraNoir?

It's a ransomware protection system based on collective perception.
This repository contains a client application of LibraNoir system.

### How does it work?

When a process try to modify a file, 

1. LibraNoirFilter(Windows filesystem minifilter driver) monitors file request from processes in user computer.
2. If the process is not trusted system process and the file extension is registered for monitoring, 
it will send the data of process and file to LibraNoir application.
3. LibraNoir application will show the received data to user to decide whether to permit or deny.
4. If user permit a process, it is now treated as a trusted process. On the other hand, if user deny a process, it goes to black list. Users can modify their policy as they want.

### Information given to user

* process
  * process name
  * process file location
  * process hash value
  * code sign

* file
  * file name, extension
  * file location

* process reputation info from cloud server

### Additional function

#### Folder lock

User can lock folder with password to protect from other process to access.

#### Sandbox

User can watch a process activity to determine whether it is malicious or not. while or after watching, user can select permit or deny it. when user deny it, the activity will roll back.
