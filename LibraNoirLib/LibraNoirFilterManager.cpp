#include "stdafx.h"
#include "LibraNoirFilterManager.h"

namespace LibraNoir {

	LibraNoirFilterManager::LibraNoirFilterManager()
	{ 
		_communicationManager.ConnectToFilter();
	}

	LibraNoirFilterManager::~LibraNoirFilterManager() { }


	BOOLEAN LibraNoirFilterManager::AddFileExtension(std::wstring fileExtension) {

		RCV_MSG msg;
		BOOLEAN success;
		
		InitMessageStruct(msg);
		wcscpy(msg.FileExtension, fileExtension.c_str());
		msg.CommandMessage = ADD_FILE_EXTENSION;

		if (!_communicationManager.SendMessageToFilter(msg, success))
			return FALSE;
		
		return success;
	}


	BOOLEAN LibraNoirFilterManager::RemoveFileExtension(std::wstring fileExtension) {

		RCV_MSG msg;
		BOOLEAN success;

		InitMessageStruct(msg);
		wcscpy(msg.FileExtension, fileExtension.c_str());
		msg.CommandMessage = REMOVE_FILE_EXTENSION;

		if (!_communicationManager.SendMessageToFilter(msg, success))
			return FALSE;

		return success;
	}

	BOOLEAN LibraNoirFilterManager::InstallProcessProctection() {

		RCV_MSG msg;
		BOOLEAN success;

		InitMessageStruct(msg);
		msg.CommandMessage = ENABLE_PROCESS_PROTECTION;

		if (!_communicationManager.SendMessageToFilter(msg, success))
			return FALSE;

		return success;
	}

	BOOLEAN LibraNoirFilterManager::UnInstallProcessProtection() {

		RCV_MSG msg;
		BOOLEAN success;

		InitMessageStruct(msg);
		msg.CommandMessage = DISABLE_PROCESS_PROTECTON;

		if (!_communicationManager.SendMessageToFilter(msg, success))
			return FALSE;

		return success;
	}

	SEND_DATA LibraNoirFilterManager::WaitMessage() {

		SEND_DATA msg;
		InitMessageStruct(msg);

		_communicationManager.GetMessageFromFilter(msg);

		return msg;
	}

	BOOLEAN LibraNoirFilterManager::ReplyMessage(ULONGLONG msgID, BOOLEAN permit) {

		REPLY_DATA msg;
		InitMessageStruct(msg);

		msg.ReplyHeader.MessageId = msgID;
		msg.Msg.IsApproved = permit;

		if (!_communicationManager.ReplyMessageToFilter(msg)) {
			return FALSE;
		}
		return TRUE;
	}

	BOOLEAN LibraNoirFilterManager::isConnected()
	{
		return _communicationManager.IsConnected();
	}
}