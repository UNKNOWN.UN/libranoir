#pragma once

#include "stdafx.h"
#include "targetver.h"

namespace LibraNoir {

	/////////	Struct for getting message from minifilter driver ///////////
	typedef struct _SEND_MSG_ {

		CHAR ProcessName[40];

		ULONG ProcessId;

		WCHAR FileExtension[30];

		WCHAR FilePath[200];

	}SEND_MSG, *PSEND_MSG;

	typedef struct _SEND_DATA_ {

		FILTER_MESSAGE_HEADER MessageHeader;

		SEND_MSG Msg;

	}SEND_DATA, *PSEND_DATA;
	//////////////////////////////////////////////////////////////////////////

	/////////	Struct for replying message to minifilter driver ///////////
	typedef struct _REPLY_MSG_ {

		BOOLEAN IsApproved;

	}REPLY_MSG, *PREPLY_MSG;

	typedef struct _REPLY_DATA_ {

		FILTER_REPLY_HEADER ReplyHeader;

		REPLY_MSG Msg;

	}REPLY_DATA, *PREPLY_DATA;
	////////////////////////////////////////////////////////////////////////

	/////////	Struct for sending message to minifilter driver ///////////
	typedef enum _Command_ {

		ADD_FILE_EXTENSION,

		REMOVE_FILE_EXTENSION,

		ENABLE_PROCESS_PROTECTION,

		DISABLE_PROCESS_PROTECTON

	} Command;

	typedef struct _RECIVED_MSG_ {

		Command CommandMessage;

		WCHAR FileExtension[30];

	}RCV_MSG, *PRCV_MSG;
	////////////////////////////////////////////////////////////////////////

	// Functions

	inline void SyncSendReply(SEND_DATA& sendData, REPLY_DATA& replyData) {

		replyData.ReplyHeader.MessageId = sendData.MessageHeader.MessageId;
	}

	inline void InitMessageStruct(SEND_DATA& data) {

		memset(data.Msg.ProcessName, 0, 40 * sizeof(CHAR));
		data.Msg.ProcessId = 0;
		memset(data.Msg.FileExtension, 0, 30 * sizeof(WCHAR));
		memset(data.Msg.FilePath, 0, 200 * sizeof(WCHAR));
	}

	inline void InitMessageStruct(REPLY_DATA& data) {

		data.Msg.IsApproved = FALSE;
	}

	inline void InitMessageStruct(RCV_MSG& data) {

		memset(data.FileExtension, 0, 30 * sizeof(WCHAR));
	}
}

