#include "stdafx.h"
#include "Communication.h"

namespace LibraNoir {

	FilterCommunicationManager::FilterCommunicationManager() : _port(NULL) {   }
	FilterCommunicationManager::~FilterCommunicationManager() {    }

	BOOLEAN FilterCommunicationManager::ConnectToFilter() {

		if (S_OK == FilterConnectCommunicationPort(
			COMMUNICATION_PORT_NAME,			// Port Name.
			0,									// Connection Option [Default: 0].
			NULL,								// Context information to be passed to minifilter's connect notification routine.
			0,									// Size of Context.
			NULL,								// SECURITY_ATTRIBUTES that determine HANDLE can be inherited to child process [0: NOT PERMITTED].
			&_port								// Communication Port return from filter manager.
		)) {
			return TRUE;
		}
		_port = NULL;
		return FALSE;
	}

	BOOLEAN FilterCommunicationManager::IsConnected() {

		return _port != NULL;
	}

	BOOLEAN FilterCommunicationManager::GetMessageFromFilter(SEND_DATA& data) {

		if (!IsConnected()) return FALSE;

		if (S_OK == FilterGetMessage(
			_port,
			&data.MessageHeader,
			sizeof(FILTER_MESSAGE_HEADER) + sizeof(SEND_MSG),
			NULL
		)) {
			return TRUE;
		}
		return FALSE;
	}

	BOOLEAN FilterCommunicationManager::ReplyMessageToFilter(REPLY_DATA& data) {

		if (!IsConnected()) return FALSE;

		if (S_OK == FilterReplyMessage(
			_port,
			&data.ReplyHeader,
			sizeof(FILTER_REPLY_HEADER) + sizeof(REPLY_MSG)
		)) {
			return TRUE;
		}
		return FALSE;
	}

	BOOLEAN FilterCommunicationManager::SendMessageToFilter(RCV_MSG& data, BOOLEAN& success) {

		ULONG bytesReturned = 0;
		BOOLEAN commandSuccess;

		if (!IsConnected()) return FALSE;

		if (S_OK == FilterSendMessage(
			_port,
			&data,
			sizeof(RCV_MSG),
			&commandSuccess,
			sizeof(BOOLEAN),
			&bytesReturned
		)) {
			success = commandSuccess;
			return TRUE;
		}
		return FALSE;
	}
}