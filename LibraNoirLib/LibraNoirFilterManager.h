#pragma once

#include "stdafx.h"
#include "targetver.h"

#include "Communication.h"
#include "Message.h"

namespace LibraNoir {

	class LibraNoirFilterManager
	{

	public:
		LibraNoirFilterManager();
		~LibraNoirFilterManager();

		BOOLEAN InstallProcessProctection();
		BOOLEAN UnInstallProcessProtection();

		BOOLEAN AddFileExtension(std::wstring fileExtension);
		BOOLEAN RemoveFileExtension(std::wstring fileExtension);
		
		SEND_DATA WaitMessage();
		BOOLEAN ReplyMessage(ULONGLONG msgID, BOOLEAN permit);

		BOOLEAN isConnected();

	private:
		FilterCommunicationManager _communicationManager;

	};

}

